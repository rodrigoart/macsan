<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use \App\Models\{User, HojaMonitoreo, Insumo, Minuta, Problema};

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(9)->create();

        User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role' => 'Administrador',
            'password' => bcrypt('123456'),
        ]);

        HojaMonitoreo::factory(20)->create();
        Insumo::factory(100)->create();
        Minuta::factory(20)->create();
        Problema::factory(100)->create();

    }
}
