<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insumos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hoja_monitoreo_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('nombre')->nullable()->nullable();
            $table->string('in_stock')->nullable()->nullable();
            $table->unsignedInteger('saldo_kardex')->nullable();
            $table->string('entrada')->nullable()->nullable();
            $table->string('fecha_vencimiento')->nullable();
            $table->unsignedInteger('lote')->nullable();
            $table->boolean('activo')->default(false);
            $table->string('med')->nullable()->nullable();
            $table->unsignedBigInteger('no_kardex')->nullable();
            $table->string('DR')->nullable()->nullable();
            $table->unsignedBigInteger('existencia_real')->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamps();

            //relations
            $table->foreign('hoja_monitoreo_id')->references('id')->on('hoja_monitoreos')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insumos');
    }
};
