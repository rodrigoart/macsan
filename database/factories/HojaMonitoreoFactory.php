<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\HojaMonitoreo>
 */
class HojaMonitoreoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
           'supervisor' => $this->faker->word(),
           'user_id' => rand(1,10),
           'actualizado_por' => rand(1,10),
           'insumos_actualizado_por' => rand(1,10),
           'ultima_actualizacion_insumos' => $this->faker->date(),
           'fecha' => $this->faker->date(),
           'municipio' => $this->faker->word(),
           'territorio' => $this->faker->word(),
           'cs' => 'Centro de Salud Dionisio Guttierez, San Cristobal Totonicapan',
           'ps' => $this->faker->word(),
           'personal_supervisado' => $this->faker->sentence(),
           'cargo' => $this->faker->word(),
           'observaciones' => $this->faker->text(180),
        ];
    }
}

            // $table->string('supervisor')->nullable();
            // $table->unsignedBigInteger('id_supervisor')->nullable();
            // $table->unsignedBigInteger('id_usuario');
            // $table->string('aprovado')->default('no');
            // $table->string('municipio');
            // $table->string('territorio');
            // $table->string('c/s')->nullable();
            // $table->string('p/s')->nullable();
            // $table->string('personal_supervisado')->nullable();
            // $table->string('cargo')->nullable();
