<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Minuta>
 */
class MinutaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'distrito' => $this->faker->word(),
            'responsable' => $this->faker->word(),
            'user_id' => rand(1,9),
            'actualizado_por' => rand(1,9),
            'supervisor' => $this->faker->word(),
            'participantes' => $this->faker->sentence(),
            'fecha_analisis' => $this->faker->date(),
            'fecha_inicio' => $this->faker->date(),
            'fecha_final' => $this->faker->date(),
            'observaciones' => $this->faker->text(180),
        ];
    }
}

            // $table->string('distrito')->nullable();
            // $table->string('responsable')->nullable();
            // $table->string('supervisor')->nullable();
            // $table->string('participantes')->nullable();
            // $table->date('fecha_inicio')->nullable();
            // $table->date('fecha_final')->nullable();
            // $table->text('observaciones')->nullable();
