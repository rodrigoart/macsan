<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Insumo>
 */
class InsumoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'hoja_monitoreo_id' => rand(1,20),
            'user_id' => rand(1,10),
            'nombre' => $this->faker->word(),
            'in_stock' => rand(0,100),
            'saldo_kardex' => rand(0,100),
            'entrada' => rand(0,100),
            'fecha_vencimiento' => $this->faker->date(),
            'lote' => rand(1,20),
            'no_kardex' => rand(300,400),
            'observaciones' => $this->faker->text(180),

        ];
    }
}

            // $table->unsignedBigInteger('id_hojaMonitoreo');
            // $table->string('nombre');
            // $table->string('in_stock');
            // $table->unsignedInteger('saldo_kardex');
            // $table->string('entrada');
            // $table->string('fecha_vencimiento');
            // $table->unsignedInteger('lote');
            // $table->string('med')->nullable();
            // $table->unsidgendBigInteger('no_kardex');
            // $table->string('DR')->nullable();
            // $table->unsigendInteger('existencia_real')->nullable();
            // $table->text('observaciones')->nullable();
