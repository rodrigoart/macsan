<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Problema>
 */
class ProblemaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'minuta_id' => rand(1,20),
            'user_id' => rand(1,9),
            'descripcion' => $this->faker->word(),
            'responsable' => $this->faker->word(),
            'causas' => $this->faker->sentence(),
            'recursos' => $this->faker->sentence(),
            'acciones' => $this->faker->sentence(),
            'fecha_aplicacion' => $this->faker->date(),
            'fecha_solucionado' => $this->faker->dateTimeBetween('2022-01-01T00:00:00.000Z', '2022-12-30T00:00:00.000Z'),
            'logros' => $this->faker->text(200),
        ];
    }
}
